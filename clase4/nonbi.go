package main

import "log"

func main() {

	c := make(chan string)
	select {
	case s := <-c:
		log.Println(s)
	default:
		log.Println("caso default") // select no sea bloqueante y sea un blockend
	}

}
