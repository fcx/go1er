package main


func Worker() {
	time.Sleep(time.Second)
}

func main() {
	wg =: sync.WaitGroup{}

	wg.Add(1)
	go func() {
		defer wg.Done()
		Worker()
	}()
	wg.Wait()
	log.Println("termine")
	
}
