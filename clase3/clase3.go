package main //errores , error es una intefaz , error es el ultimo parametro // tratatar errorres primero // go and defer llamando a una funcion
import "log"

type pares struct {
	a, b int
}

func Multiplicar(jobs chan pares, results chan int) {
	for p := range jobs {
		results <- p.a * p.b
	}
	close(results)
}

func main() {
	jobs := make(chan pares, 10)
	results := make(chan int, 10)

	go Multiplicar(jobs, results)

	for i := 0; i < 10; i++ {
		jobs <- pares{a: i, b: i + 1}
	}
	close(jobs)

	for r := range results {
		log.Println(r)
	}
}
