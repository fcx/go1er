package main

import (
	"fmt"
	"math"
)

type Circulo struct {
	Radio float64
	X, Y  int
}

func (c Circulo) Area() float64 {
	return math.Pi * math.Pow(c.Radio, 2)
}

func (c *Circulo) SetRadio(newRadio float64) {
	c.Radio = newRadio
}

func main() {
	c := Circulo{}

	c.Radio = 90.0

	fmt.Println(c.Radio, c.Area())

	c.SetRadio(100)

	fmt.Println(c.Radio)
}
